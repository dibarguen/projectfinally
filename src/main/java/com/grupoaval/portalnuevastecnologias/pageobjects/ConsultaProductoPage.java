package com.grupoaval.portalnuevastecnologias.pageobjects;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://aae49f8cf479211eaad3c0ed862d58a0-8482866.us-east-1.elb.amazonaws.com/")
public class ConsultaProductoPage extends PageObject {

	// Escenario 2

	By buttonComprarInicioIrAProductos = By.xpath("//a[contains(.,'Comprar')]");

	// Escenario 4

	By buttonConsultarPrecio = By.xpath("//button[contains(.,'search')]");

	// Escenario 5

	By checkBoxDisponibleFiltro = By.xpath("//span[contains(.,'Disponible')]");

	By checkBoxNoDisponibleFiltro = By.xpath("//span[contains(.,'No Disponible')]");

	By buttonConsultarDisponibilidad = By.xpath("//div/div[2]/button");

	// Escenario 7

	By buttonVistaRapidaPrimerProducto = By.xpath("//button[contains(.,'remove_red_eye')]");

	// Escenario 8

	By buttonViewFullDetails = By.xpath("//button[contains(.,'arrow_forward')]");

	// Escenario 9

	By buttonCerrarVistaRapida = By.xpath("//app-product-dialog/div/button");

	// Escenario 1

	public void clicEnBotonDeCategoriaMenuSuperior(String categoria) {
		String btnCategoria = "(//a[contains(@href, '/products/" + categoria + "')])[2]";
		find(btnCategoria).click();
	}

	public void validarExistenciaProductoCategoriaSeleccioanda(String nombreProducto) {
		WebElement aMensajeValidar = getDriver().findElement(By.xpath("//a[contains(.,'" + nombreProducto + "')]"));
		System.out.println("Nombre: " + aMensajeValidar.getText());
		if (aMensajeValidar.isDisplayed()) {
			assertThat(aMensajeValidar.getText(), equalTo(nombreProducto));
		} else {
			throw new AssertionError("El elemento " + aMensajeValidar.toString() + " con mensaje '" + nombreProducto
					+ "' no está presente en la pantalla", null);
		}
	}

	// Escenario 2

	public void clickComprarIrAProductos() {
		find(buttonComprarInicioIrAProductos).click();
	}

	public void clicEnBotonDeCategoriaMenuIzquierdo(String categoria) {
		String btnCategoriaIzquierda = "";
		if (categoria.equals("celulares")) {
			btnCategoriaIzquierda = "//div[2]/button/span";
		} else if (categoria.equals("audio")) {
			btnCategoriaIzquierda = "//div[3]/button/span";
		} else if (categoria.equals("accesorios")) {
			btnCategoriaIzquierda = "//div[4]/button/span";
		} else if (categoria.equals("todas las categorias")) {
			btnCategoriaIzquierda = "//span[contains(.,'Todas las Categorias')]";
		} else {
			throw new AssertionError("La categoria: " + categoria + " no existe", null);
		}
		find(btnCategoriaIzquierda).click();
		espera(5000);
	}

	// Escenario 3

	public void clicEnBotonDeCategoriaMenuInferiorInicial(String categoria) {
		// Scroll Inicial
		try {
			Scroll(0, 1600);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		espera(1000);
		// click Categoria
		String clickLabelCategoria = "";
		if (categoria.equals("celulares")) {
			clickLabelCategoria = "//mat-tab-header/div[2]/div/div/div/div";
		} else if (categoria.equals("audio")) {
			clickLabelCategoria = "//div[2]/div/div/div[2]/div";
		} else if (categoria.equals("accesorios")) {
			clickLabelCategoria = "//div[2]/div/div/div[3]/div']";
		} else {
			throw new AssertionError("La categoria: " + categoria + " no existe", null);
		}
		find(clickLabelCategoria).click();
		espera(4000);
	}

	// Escenario 4

	public void seleccionarValorPrecioDesde(Integer precioDesde) {
		espera(1000);
		WebElement valorDesde = getDriver().findElement(By.xpath("//div[3]/div[2]"));
		Actions move = new Actions(getDriver());
		Action action = (Action) move.dragAndDropBy(valorDesde, precioDesde, 1).build();
		action.perform();
		espera(1000);
	}

	public void seleccionarValorPrecioHasta(Integer precioHasta) {
		espera(1000);
		WebElement valorHasta = getDriver().findElement(By.xpath("//mat-slider[2]/div/div[3]/div[2]"));
		Actions move = new Actions(getDriver());
		Action action = (Action) move.dragAndDropBy(valorHasta, precioHasta, 1).build();
		action.perform();
		espera(1000);
	}

	public void clickButtonConsultarPrecio() {
		find(buttonConsultarPrecio).click();
		espera(3000);
	}

	// Escenario 5

	public void seDirigeAOpcionDisponiblidadYLaSelecciona(String disponiblidad) {
		try {
			Scroll(0, 300);
		} catch (Exception e) {
			e.printStackTrace();
		}
		espera(3000);
		// Click disponible - no disponible
		if (disponiblidad.equals("disponible")) {
			find(checkBoxDisponibleFiltro).click();
		} else if (disponiblidad.equals("no disponible")) {
			find(checkBoxNoDisponibleFiltro).click();
		} else {
			throw new AssertionError("CheckBox de disponibilidad erroneo", null);
		}
	}

	public void clickButtonConsultarDisponibilidad() {
		find(buttonConsultarDisponibilidad).click();
	}

	// Escenario 7

	public void clickButtonVistaRapidaPrimerProducto() {
		find(buttonVistaRapidaPrimerProducto).click();
	}

	public void validarNombrePrimerProductoVistaRapida(String nombrePrimerProducto) {
		espera(1000);
		WebElement lblMensaje = getDriver().findElement(By.xpath("//h2[contains(.,'" + nombrePrimerProducto + "')]"));
		if (lblMensaje.isDisplayed()) {
			assertThat(lblMensaje.getText(), equalTo(nombrePrimerProducto));
		} else {
			throw new AssertionError("El elemento " + lblMensaje.toString() + " con mensaje '" + nombrePrimerProducto
					+ "' no está presente en la pantalla", null);
		}
		espera(1000);
	}

	// Escenario 8

	public void clickButtonViewFullDetails() {
		find(buttonViewFullDetails).click();
		espera(2000);
	}

	// Escenario 9

	public void clickButtonCerrarVistaRapida() {
		find(buttonCerrarVistaRapida).click();
		espera(2000);
	}

	public void validarPresenciaYContenidoDeMensaje(String mensaje) {
		waitForAngularRequestsToFinish();
		WebElement lblMensaje = getDriver().findElement(By.xpath("//*[text()='" + mensaje + "']"));
		if (lblMensaje.isDisplayed()) {
			assertThat(lblMensaje.getText(), equalTo(mensaje));
		} else {
			throw new AssertionError("El elemento " + lblMensaje.toString() + " con mensaje '" + mensaje
					+ "' no está presente en la pantalla", null);
		}
	}

	public void validarMensajeRegistroUsuario(String mensaje) {
		validarPresenciaYContenidoDeMensaje(mensaje);
	}

	public void Scroll(int ParametroInt1, int ParametroInt2) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(" + ParametroInt1 + "," + ParametroInt2 + ")", "");
	}

	public void espera(int milisegundos) {
		try {
			Thread.sleep(milisegundos);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
